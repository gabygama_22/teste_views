# coding=utf-8
from autenticacao.views import *
import pytest
from django.test import Client
import mock
from django.contrib.auth.models import User
import logging
from django.test import TestCase, RequestFactory

"""@pytest.mark.django_db
def test_login_view_post_false(monkeypatch):

	username = 'teste'
	password = 'teste'
	user = authenticate(username=username, password=password)
	assert  login(request,user) is False"""

@pytest.fixture
def coisa(client):
	user = User(username="asdf",password="1234")
	user.set_password('123456')
	user.save()
	client.login(username=user.username, password="123456")
	return user,client

@pytest.mark.django_db
def test_coisa(client):
	user,client = coisa(client)

	assert user is not None

@pytest.mark.django_db
def test_login_view_get(client):
	response = client.get('/login/')
	assert 200 <= response.status_code < 300
	response_chain = client.get('/login/', follow=True)
	assert response_chain.redirect_chain == [('/login/', 301)]


"""
@pytest.mark.django_db
def test_login_view_post():
	rf = RequestFactory()
	user = User.objects.create_user(username='test_nome',password='123456')
	post_request = rf.post('/perfil/')
	assert post_request.status_code is 200
"""

def test_checar_vazio_true():
	variaveis = ['teste']
	assert checar_vazio(variaveis)

def test_checar_vazio_false():
	variaveis = ['']
	assert checar_vazio(variaveis) is False

def test_checar_confirmacao():
	return checar_confirmacao('teste','teste')

def test_registro_view_get():
	client = Client()
	response = client.get('/cadastro/')
	assert response.status_code is 200
